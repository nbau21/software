## Install Emacs
EMACS=${PWD}/emacs
UNAME := $(shell uname)
ifeq ($(UNAME), Linux)
	TAR := tar
endif
ifeq ($(UNAME), Darwin)
	TAR := gtar
endif

emacs:
	@echo "Installing Emacs"
	${TAR} -xzvf emacs.tar.gz --one-top-level=${EMACS} --strip-components 1
	cd ${EMACS}; ${EMACS}/autogen.sh; ${EMACS}/configure --prefix=${HOME}/.local --bindir=${HOME}/bin; make; make install
	rm -rf ${EMACS}
	@echo "Finished installing Emacs"

emacsdesktopentry:
	@echo "Creating desktop entry for Emacs"
	cp ${PWD}/desktop/emacs.desktop ${HOME}/.local/share/applications/emacs.desktop
	echo "Exec=${HOME}/bin/emacs" >> ${HOME}/.local/share/applications/emacs.desktop

## Install Emacs Plugins
emacsplugins: evil diredhacks diredsidebar whichkey purpose adoc company kotlinmode luamode gomode companygo projectile magit evilmagit

### Evil
evil:
	@echo "Extracting evil to ~/.emacs.d/evil"
	${TAR} -xzvf evil.tar.gz --one-top-level=${HOME}/.emacs.d/evil --strip-components 1
	@echo "Extracted evil to ~/.emacs.d/evil"

### Dash
dash:
	@echo "Extracting dash to ~/.emacs.d/dash.el"
	${TAR} -xzvf dash.el.tar.gz --one-top-level=${HOME}/.emacs.d/dash.el --strip-components 1
	@echo "Extracted dash to~/.emacs.d/dash.el"

### Dired-Hacks
diredhacks:
	@echo "Extracting dired-hacks to ~/.emacs.d/dired-hacks"
	${TAR} -xzvf dired-hacks.tar.gz --one-top-level=${HOME}/.emacs.d/dired-hacks --strip-components 1
	@echo "Extracted dired-hacks to~/.emacs.d/dired-hacks"

### Dired-Sidebar
diredsidebar:
	@echo "Extracting dired-sidebar to ~/.emacs.d/dired-sidebar"
	${TAR} -xzvf dired-sidebar.tar.gz --one-top-level=${HOME}/.emacs.d/dired-sidebar --strip-components 1
	@echo "Extracted dired-sidebar to~/.emacs.d/dired-sidebar"

### With-Editor
witheditor:
	@echo "Extracting with-editor to ~/.emacs.d/with-editor"
	${TAR} -xzvf with-editor.tar.gz --one-top-level=${HOME}/.emacs.d/with-editor --strip-components 1
	@echo "Extracted with-editor to~/.emacs.d/with-editor"

### Which-Key
whichkey:
	@echo "Extracting which-key to ~/.emacs.d/which-key"
	${TAR} -xzvf which-key.tar.gz --one-top-level=${HOME}/.emacs.d/which-key --strip-components 1
	@echo "Extracted with-editor to~/.emacs.d/which-key"

### Hydra
hydra:
	@echo "Extracting hydra to ~/.emacs.d/hydra"
	${TAR} -xzvf hydra.tar.gz --one-top-level=${HOME}/.emacs.d/hydra --strip-components 1
	@echo "Extracted hydra to~/.emacs.d/hydra"

### Transient
transient:
	@echo "Extracting transient to ~/.emacs.d/transient"
	${TAR} -xzvf transient.tar.gz --one-top-level=${HOME}/.emacs.d/transient --strip-components 1
	@echo "Extracted transient to~/.emacs.d/transient"

### Magit-popup
magitpopup:
	@echo "Extracting magit-popup to ~/.emacs.d/magit-popup"
	${TAR} -xzvf magit-popup.tar.gz --one-top-level=${HOME}/.emacs.d/magit-popup --strip-components 1
	@echo "Extracted magit-popup to~/.emacs.d/magit-popup"

### GraphQL
graphql:
	@echo "Extracting graphql to ~/.emacs.d/graphql"
	${TAR} -xzvf graphql.el.tar.gz --one-top-level=${HOME}/.emacs.d/graphql --strip-components 1
	@echo "Extracted graphql to~/.emacs.d/graphql"

### Ghub
ghub:
	@echo "Extracting ghub to ~/.emacs.d/ghub"
	${TAR} -xzvf ghub.tar.gz --one-top-level=${HOME}/.emacs.d/ghub --strip-components 1
	@echo "Extracted ghub to~/.emacs.d/ghub"

### Treepy
treepy:
	@echo "Extracting treepy to ~/.emacs.d/treepy"
	${TAR} -xzvf treepy.tar.gz --one-top-level=${HOME}/.emacs.d/treepy --strip-components 1
	@echo "Extracted treepy to~/.emacs.d/treepy"

### Magit
magit: witheditor hydra transient dash ghub graphql magitpopup treepy
	@echo "Extracting magit to ~/.emacs.d/magit"
	${TAR} -xzvf magit.tar.gz --one-top-level=${HOME}/.emacs.d/magit --strip-components 1
	touch ${HOME}/.emacs.d/magit/config.mk
	echo "LOAD_PATH = ${HOME}/.emacs.d/magit/lisp; ${HOME}/.emacs.d/dash.el; ${HOME}/.emacs.d/hydra; ${HOME}/.emacs.d/transient; ${HOME}/.emacs.d/with-editor" >> ${HOME}/.emacs.d/magit/config.mk
	echo "EMACSBIN = ${HOME}/bin/emacs" >> ${HOME}/.emacs.d/magit/config.mk
	cd ${HOME}/.emacs.d/magit; make
	@echo "Extracted magit to~/.emacs.d/magit"

### Evil-Magit
evilmagit:
	@echo "Extracting evil-magit to ~/.emacs.d/evil-magit"
	${TAR} -xzvf evil-magit.tar.gz --one-top-level=${HOME}/.emacs.d/evil-magit --strip-components 1
	@echo "Extracted evil-magit to~/.emacs.d/evil-magit"

### imenu list
imenu:
	@echo "Extracting imenu-list to ~/.emacs.d/imenu-list"
	${TAR} -xzvf imenu-list.tar.gz --one-top-level=${HOME}/.emacs.d/imenu-list --strip-components 1
	@echo "Extracted imenu-list to~/.emacs.d/imenu-list"

### Emacs Purpose
purpose: imenu
	@echo "Extracting emacs-purpose to ~/.emacs.d/emacs-purpose"
	${TAR} -xzvf emacs-purpose.tar.gz --one-top-level=${HOME}/.emacs.d/emacs-purpose --strip-components 1
	@echo "Extracted emacs-purpose to~/.emacs.d/emacs-purpose"

### Markup-Faces
markupfaces:
	@echo "Extracting markup-faces to ~/.emacs.d/markup-faces"
	${TAR} -xzvf markup-faces.tar.gz --one-top-level=${HOME}/.emacs.d/markup-faces --strip-components 1
	@echo "Extracted markup-faces to~/.emacs.d/markup-faces"

### Adoc
adoc: markupfaces
	@echo "Extracting adoc to ~/.emacs.d/adoc-mode"
	${TAR} -xzvf adoc-mode.tar.gz --one-top-level=${HOME}/.emacs.d/adoc-mode --strip-components 1
	@echo "Extracted adoc to~/.emacs.d/adoc-mode"

### Company (Auto-complete)
company:
	@echo "Extracting company to ~/.emacs.d/company"
	${TAR} -xzvf company.tar.gz --one-top-level=${HOME}/.emacs.d/company --strip-components 1
	@echo "Extracted company to~/.emacs.d/company"

### Kotlin Mode
kotlinmode:
	@echo "Extracting kotlin-mode to ~/.emacs.d/kotlin-mode"
	${TAR} -xzvf kotlin-mode.tar.gz --one-top-level=${HOME}/.emacs.d/kotlin-mode --strip-components 1
	@echo "Extracted kotlin-mode to~/.emacs.d/kotlin-mode"

### Lua Mode
luamode:
	@echo "Extracting lua-mode to ~/.emacs.d/lua-mode"
	${TAR} -xzvf lua-mode.tar.gz --one-top-level=${HOME}/.emacs.d/lua-mode --strip-components 1
	@echo "Extracted lua-mode to~/.emacs.d/lua-mode"

### Go Mode
gomode:
	@echo "Extracting go-mode to ~/.emacs.d/go-mode"
	${TAR} -xzvf go-mode.tar.gz --one-top-level=${HOME}/.emacs.d/go-mode --strip-components 1
	@echo "Extracted go-mode to~/.emacs.d/go-mode"

### Go Syntax checker
companygo:
	@echo "Extracting company-go to ~/.emacs.d/company-go"
	${TAR} -xzvf company-go.tar.gz --one-top-level=${HOME}/.emacs.d/company-go --strip-components 1
	@echo "Extracted company-go to~/.emacs.d/company-go"

### Projectile
projectile:
	@echo "Extracting projectile to ~/.emacs.d/projectile"
	${TAR} -xzvf projectile.tar.gz --one-top-level=${HOME}/.emacs.d/projectile --strip-components 1
	@echo "Extracted projectile to~/.emacs.d/projectile"

uemacs:
	@echo "Uninstalling Emacs.."
	rm -rf "${HOME}/bin/emacs"
	rm -f ${HOME}/.local/lib/systemd/user/emacs.service
	rm -rf ${HOME}/.local/libexec/emacs/
	rm -f ${HOME}/.local/share/appdata/emacs.appdata.xml
	rm -f ${HOME}/.local/share/icons/hicolor/128x128/apps/emacs.png
	rm -f ${HOME}/.local/share/icons/hicolor/16x16/apps/emacs.png
	rm -f ${HOME}/.local/share/icons/hicolor/24x24/apps/emacs.png
	rm -f ${HOME}/.local/share/icons/hicolor/32x32/apps/emacs.png
	rm -f ${HOME}/.local/share/icons/hicolor/48x48/apps/emacs.png
	rm -f ${HOME}/.local/share/icons/hicolor/scalable/apps/emacs.svg
	rm -rf ${HOME}/.local/share/icons/hicolor/scalable/mimetypes/emacs-document/
	rm -rf ${HOME}/.local/share/emacs
	@echo "Deleting Emacs configs"
	rm -rf ${HOME}/.emacs.d/
	@echo "Deleted Emacs configs"

uninstall: uemacs
